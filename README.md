
# ci.test02

<!-- badges: start -->
<!-- badges: end -->

The goal of ci.test02 is to ...

## Installation

You can install the released version of ci.test02 from [CRAN](https://CRAN.R-project.org) with:

``` r
install.packages("ci.test02")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(ci.test02)
## basic example code
```

